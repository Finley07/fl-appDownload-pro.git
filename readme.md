## App下载发行页

#### 下载导入后修改以下参数↓

##### pages/index/index.vue

|  参数名      |    类型   |     说明      |
| :-------------: | :--------: | :---------------------------------------------------: |
| name | String |       app名称|
| des | String |    app简述    |
|title| String |    页面顶部标题    |
| logoSrc| String |  logo地址 |
| bg| String |      背景 可选背景色或背景图片      |
| bgVague | String |背景毛玻璃效果        |
| imgSrc| Array|      应用截图的图片地址 |
| swiperTime| Number|      应用截图轮播间隔时间(毫秒) |
| apkSrc| String |      安卓安装包网络地址 或应用市场链接|
| ipaSrc| Number|      苹果 App Store 应用id|
| hapSrc| String |      鸿蒙原生 应用id |

#### 部署流程：

以下三种方式均可⬇

1. 运行基础路径填写为相对路径（./）且网站域名置空。（编译文件可在任何位置直接打开）
2. 填写运行基础路径与网站域名，将编译文件放置在服务器对应目录下。（只能部署到服务器使用，不能使用资源管理器直接打开）
3. 配置uniCloud前端页面托管：[uniCloud 前端网页托管](https://doc.dcloud.net.cn/uniCloud/hosting.html#host-uni-app)

#### 页面预览示例：
![fl-appDownload-pro.png](https://raw.gitcode.com/Finley07/fl-appDownload-pro/attachment/uploads/b6e9cd3f-c671-4f19-87b3-f7a1d8804266/fl-appDownload-pro.png 'fl-appDownload-pro.png')

[http://template.finley.xin/fl-template-news](http://template.finley.xin/fl-template-news)
#### 其他

关于广告：开源不易，生活不易，广告下载，实属无奈，理解万岁。<br/>
我将陆续推出其他项目及页面模板，旨在方便大家的开发工作。


End... Good luck us

© Created by Finley

